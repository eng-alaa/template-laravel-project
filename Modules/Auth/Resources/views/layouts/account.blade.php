<!DOCTYPE html>
<html>
    <head lang="{{ LaravelLocalization::setLocale() }}">
        @section('meta')
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="title" content="{{ trans('midade/Overrides::Overrides/common.site_name') }}" />
        <meta name="description" content="{{ trans('midade/Overrides::Overrides/common.site_description') }}" />
        @show
        {{--Title--}}
        <title>
            @section('title')
                {{ trans('midade/Overrides::Overrides/common.site_name') }}
        </title>

        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
        {{-- <link rel="stylesheet" href="{{ theme_url('css/style.css') }}"> --}}
       
        {{-- Bind inline scripts --}}
        @yield('styles')

    </head>

<body class="{{ LaravelLocalization::getCurrentLocaleDirection() == "rtl" ? "rtl-mode" : "" }} hold-transition login-page">


        <div id="wrapper">
            


            @include('partials.header')

            <div class="login-box">
                    @yield('page')
                </div>
            @include('partials.footer')


        </div>
    {{-- Queue js assets --}}
    {{ Asset::queue('jquery', 'vendor/jquery/jquery.js') }}
    {{ Asset::queue('bootstrap', 'vendor/bootstrap/js/bootstrap.min.js','jquery') }}
    {{ Asset::queue('icheck', 'vendor/iCheck/icheck.min.js','jquery') }}
    {{ Asset::queue('alertify', 'js/vendor/alertify/alertify.js','jquery') }}

    {{-- Compile Css Assets --}}
    @foreach(Asset::allJs() as $js)
        <script src="{{ asset($js) }}" type="text/javascript"></script>
    @endforeach

    {{-- Bind inline scripts --}}
    @yield('scripts')



</body>
</html>
