<!DOCTYPE html>
<html>

    {{-- <head lang="{{ LaravelLocalization::setLocale() }}"> --}}
        <head lang="en">


        @section('meta')
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="title" content="{{ trans('midade/Overrides::Overrides/common.site_name') }}" />
        <meta name="description" content="{{ trans('midade/Overrides::Overrides/common.site_description') }}" />

        @show

        <title>
            @section('title'){{ trans('midade/Overrides::Overrides/common.site_name') }}@show
        </title>
          <!--     Fonts and icons     -->
        <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900|Roboto+Slab:400,700" />
        <!-- Nucleo Icons -->
        <link href="{{ asset('themes/admin-child/assets/css/nucleo-icons.css') }}" rel="stylesheet" />
        <link href="{{ asset('themes/admin-child/assets/css/nucleo-svg.css') }}" rel="stylesheet" />
        <!-- Font Awesome Icons -->
        <script src="https://kit.fontawesome.com/42d5adcbca.js" crossorigin="anonymous"></script>
        <!-- Material Icons -->
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons+Round" rel="stylesheet">
        <!-- CSS Files -->
        <link id="pagestyle" href="{{ asset('themes/admin-child/assets/css/material-dashboard.css?v=3.0.0') }}" rel="stylesheet" />

      
        @yield('styles')
    </head>

    {{-- <body class="{{ LaravelLocalization::getCurrentLocaleDirection() == "rtl" ? "rtl-mode" : "" }}"> --}}
         <body class="">

        <div id="wrapper">
            <div id="overlayer"></div> 


            @include('auth::partials.header')

            @yield('page')
            @if(Session::has('flash_message_error'))
                <div class="alert alert-danger alert-block">
                    <strong>{!!session('flash_message_error')!!}</strong>
                </div>
            @endif
            
            @if(Session::has('flash_message_success'))
                <div class="alert alert-success alert-block">
                    <strong>{!!session('flash_message_success')!!}</strong>
                </div>
            @endif
            @include('auth::partials.footer')
        </div>

        @include('auth::partials.scripts')
        @yield('scripts')

    </body>

</html>