<?php

namespace Modules\Auth\Http\Controllers\WEB;
use App\Http\Controllers\Controller;
use App\Repositories\BaseRepository;
use Illuminate\Http\Request;
use Modules\Auth\Http\Requests\Backend\Role\DeleteRoleRequest;
use Modules\Auth\Repositories\Permission\PermissionRepository;
use Modules\Auth\Repositories\Role\RoleRepository;

class RoleController extends Controller
{
    /**
     * @var BaseRepository
     */
    protected $baseRepo;
    /**
     * @var RoleRepository
     */
    protected $roleRepo;
    
    /**
     * @var PermissionRepository
     */
    protected $permissionRepo;

  /**
     * RolesController constructor.
     *
     * @param RoleRepository $roles
     */
    public function __construct(BaseRepository $baseRepo,RoleRepository $roleRepo,PermissionRepository $permissionRepo)
    {
        $this->roleRepo = $roleRepo;
        $this->permissionRepo = $permissionRepo;
        $this->baseRepo = $baseRepo;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
        $roles=$this->roleRepo->all();
        return view('auth::roles.index',compact('roles'));
    }

    // methods for trash
    public function trash(){
        $roles=$this->roleRepo->trash();
        return view('auth::roles.trash',compact('roles'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $permissions=$this->permissionRepo->all();
        $statuses = $this->baseRepo->getStatuses();
        return view('auth::roles.create',compact('permissions','statuses'));
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->roleRepo->store($request);
        return redirect()->route('admin.roles.create')->with('flash_message_success','created successfully');

        
    

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
        $role=$this->roleRepo->find($id);
        return view('auth::roles.show',compact('role'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $role=$this->roleRepo->find($id);
        $permissionsRole=$this->permissionRepo->permissionsRole($role);
        $permissions=$this->permissionRepo->all();
        $statuses = $this->baseRepo->getStatuses();
        return view('auth::roles.edit',compact('role','permissions','permissionsRole','statuses'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {   
        $this->roleRepo->update($request,$id);
        return redirect()->route('admin.roles.edit',$id)->with('flash_message_success','updated successfully');
    }
    
    //methods for restoring
    public function restore($id){
        $this->roleRepo->restore($id);
        return redirect()->route('admin.roles.trash')->with('flash_message_success','restored successfully');

    }
    public function restoreAll(){
        $this->roleRepo->restoreAll();
        return redirect()->route('admin.roles.trash')->with('flash_message_success','restored all successfully');

    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(DeleteRoleRequest $request,$id)
    {
        $this->roleRepo->destroy($id);
        return redirect()->route('admin.roles.index')->with('flash_message_success','deleted successfully, you can see it in trash');

    }
    public function forceDelete(DeleteRoleRequest $request,$id)
    {
        $this->roleRepo->forceDelete($id);
        return redirect()->route('admin.roles.trash')->with('flash_message_success','force deleted successfully');

    }
}
