<?php

namespace Modules\Auth\Http\Controllers\WEB;
use App\Http\Controllers\Controller;
use Modules\Auth\Http\Requests\Backend\User\StoreUserRequest;
use Modules\Auth\Http\Requests\Backend\User\UpdateUserRequest;
use Modules\Auth\Http\Requests\Backend\User\EditUserRequest;
use Modules\Auth\Http\Requests\Backend\User\CreateUserRequest;
use Modules\Auth\Http\Requests\Backend\User\DeleteUserRequest;
use Modules\Auth\Repositories\User\UserRepository;
use Modules\Auth\Repositories\Role\RoleRepository;
use Modules\Auth\Repositories\Permission\PermissionRepository;
use App\Repositories\BaseRepository;

class UserController extends Controller
{
    /**
     * @var BaseRepository
     */
    protected $baseRepo;
    /**
     * @var UserRepository
     */
    protected $userRepo;
    /**
     * @var RoleRepository
     */
    protected $roleRepo;
    
    /**
     * @var PermissionRepository
     */
    protected $permissionRepo;


    
    /**
     * UsersController constructor.
     *
     * @param UserRepository $users
     */
    public function __construct(BaseRepository $baseRepo, UserRepository $userRepo,RoleRepository $roleRepo,PermissionRepository $permissionRepo)
    {
        $this->userRepo = $userRepo;
        $this->roleRepo = $roleRepo;
        $this->permissionRepo = $permissionRepo;
        $this->baseRepo = $baseRepo;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
        $users=$this->userRepo->all();
        return view('auth::users.index',compact('users'));
    }

    // methods for trash
    public function trash(){
        $users=$this->userRepo->trash();
        return view('auth::users.trash',compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(CreateUserRequest $request)
    {
        $roles=$this->roleRepo->all();
        $permissions=$this->permissionRepo->all();
        $statuses = $this->baseRepo->getStatuses();
        return view('auth::users.create',compact('roles','permissions','statuses'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreUserRequest $request)
    {
        $this->userRepo->store($request);

        return redirect()->route('admin.users.create')->with('flash_message_success','created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user=$this->userRepo->find($id);
        
        return view('auth::users.show',compact('users'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(EditUserRequest $request,$id)
    {
        $user=$this->userRepo->find($id);
        $rolesUser=$this->roleRepo->rolesUser($user);
        $permissionsUser=$this->permissionRepo->permissionsUser($user);
        $statuses = $this->baseRepo->getStatuses();
        $roles=$this->roleRepo->all();
        $permissions=$this->permissionRepo->all();
        return view('auth::users.edit',compact('roles','permissions','user','permissionsUser','rolesUser','statuses'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateUserRequest $request, $id)
    {
        $this->userRepo->update($request,$id);
       
        return redirect()->route('admin.users.edit',$id)->with('flash_message_success','updated successfully');

    }

    //methods for restoring
    public function restore($id){
        $this->userRepo->restore($id);
        return redirect()->route('admin.users.trash')->with('flash_message_success','restored successfully');

    }
    public function restoreAll(){
        $this->userRepo->restoreAll();
        return redirect()->route('admin.users.trash')->with('flash_message_success','restored all successfully');

    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(DeleteUserRequest $request,$id)
    {
        $this->userRepo->destroy($id);
        return redirect()->route('admin.users.index')->with('flash_message_success','deleted successfully, you can see it in trash');

    }
    public function forceDelete(DeleteUserRequest $request,$id)
    {
        $this->userRepo->forceDelete($id);
        return redirect()->route('admin.users.trash')->with('flash_message_success','force deleted successfully');

    }
}
