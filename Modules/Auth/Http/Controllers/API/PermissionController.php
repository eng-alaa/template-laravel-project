<?php

namespace Modules\Auth\Http\Controllers\API;
use App\Http\Controllers\Controller;
use Modules\Auth\Entities\Permission as ModelsPermission;
use Illuminate\Http\Request;

class PermissionController extends Controller
{
    
    public function __construct()
    {
        $this->middleware(['permission:permissions_read'])->only('index');
        $this->middleware(['permission:permissions_show'])->only('show');
        $this->middleware(['permission:permissions_store'])->only('store');
        $this->middleware(['permission:permissions_update'])->only('update');
        $this->middleware(['permission:permissions_delete'])->only('delete');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
        $permissions=ModelsPermission::get();
        return \response()->json([
            'status'=>200,
            'data'=>$permissions
        ]);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $permission=  ModelsPermission::create($request->all());
        $permission->roles()->attach($request->roles);
        
        return \response()->json([
            'status'=>200,
            'message'=>'created successfully'
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $permission=ModelsPermission::findOrFail($id);
        return \response()->json([
            'status'=>200,
            'data'=>$permission
        ]);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $permission=ModelsPermission::findOrFail($id);
        $permission->update($request->all());
       $permission->roles()->sync($request->roles);

       return \response()->json([
            'status'=>200,
            'message'=>'updated successfully'
        ]);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $permission=ModelsPermission::findOrFail($id);
        $permission->roles()->detach($permission->roles);
        $permission->delete($permission);

        return \response()->json([
            'status'=>200,
            'message'=>'deleted successfully'
        ]);
    }
}
