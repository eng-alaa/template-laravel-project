<?php

namespace Modules\Auth\Http\Controllers\API;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Modules\Auth\Entities\User as ModelsUser;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    
    public function __construct()
    {
        $this->middleware(['permission:users_read'])->only('index');
        $this->middleware(['permission:users_show'])->only('show');
        $this->middleware(['permission:users_store'])->only('store');
        $this->middleware(['permission:users_update'])->only('update');
        $this->middleware(['permission:users_delete'])->only('delete');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
        $users=ModelsUser::get();
        return \response()->json([
            'status'=>200,
            'data'=>$users
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data=$request->all();
        $password=Hash::make($data['password']);
        $data['password']=$password;
        $user= ModelsUser::create($data);
        // $user->attachRoles($request->roles);//to create roles for a user
        $user->roles()->attach($request->roles);//to create roles for a user
        $user->permissions()->attach($request->permissions);//to create permissions for a user
        return \response()->json([
            'status'=>200,
            'message'=>'created successfully'
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user=ModelsUser::findOrFail($id);
        return \response()->json([
            'status'=>200,
            'data'=>$user
        ]);
    }



    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user=ModelsUser::findOrFail($id);
        $data= $request->all();
        $password=Hash::make($data['password']);
        $data['password']=$password;
        $user->update($data);
        $user->syncRoles($request->roles);//to update roles a user
        //$user->roles()->sync($request->roles);//to update roles a user
        $user->permissions()->sync($request->permissions);//to update permissions for a user

        return \response()->json([
            'status'=>200,
            'message'=>'updated successfully'
        ]);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user=ModelsUser::findOrFail($id);
        $user->detachRoles($user->roles);
        $user->detachPermissions($user->permissions);
        $user->delete($user);
        return \response()->json([
            'status'=>200,
            'message'=>'deleted successfully'
        ]);
    }
}
