<?php

namespace Modules\Auth\Http\Controllers\API;
use App\Http\Controllers\Controller;
use Modules\Auth\Entities\Role as ModelsRole;
use Illuminate\Http\Request;

class RoleController extends Controller
{
    
    public function __construct()
    {
        $this->middleware(['permission:roles_read'])->only('index');
        $this->middleware(['permission:roles_show'])->only('show');
        $this->middleware(['permission:roles_store'])->only('store');
        $this->middleware(['permission:roles_update'])->only('update');
        $this->middleware(['permission:roles_delete'])->only('delete');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
        $roles=ModelsRole::get();
        return \response()->json([
            'status'=>200,
            'data'=>$roles
        ]);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       $role= ModelsRole::create($request->all());
       $role->permissions()->attach($request->permissions);
        return \response()->json([
            'status'=>200,
            'message'=>'created successfully'
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $role=ModelsRole::findOrFail($id);
        return \response()->json([
            'status'=>200,
            'data'=>$role
        ]);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $role=ModelsRole::findOrFail($id);
        $role->update($request->all());
        $role->permissions()->sync($request->permissions);

        return \response()->json([
            'status'=>200,
            'message'=>'updated successfully'
        ]);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $role=ModelsRole::findOrFail($id);
        $role->detachPermissions($role->permissions);
        $role->delete($role);

        return \response()->json([
            'status'=>200,
            'message'=>'deleted successfully'
        ]);
    }
}
