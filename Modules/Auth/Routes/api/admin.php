<?php

use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/**************************Routes Admin***************************** */
Route::prefix('admin')->middleware(['auth:sanctum'])->namespace('Backend\API')->group(function(){
    Route::resource('users', UserController::class,['except'=>['create','edit']]);
    Route::resource('roles', RoleController::class,['except'=>['create','edit']]);
    Route::resource('permissions', PermissionController::class,['except'=>['create','edit']]);
});

require __DIR__.'/user.php';

