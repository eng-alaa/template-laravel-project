<?php
namespace Modules\Auth\Repositories\User;
use Modules\Auth\Entities\User;
use Illuminate\Support\Facades\Hash;
use Modules\Auth\Repositories\User\UserRepositoryInterface;
class UserRepository implements UserRepositoryInterface
{
   public function all(){
        $users=User::all();
        return  $users;

    }
    public  function trash(){
        $users=User::onlyTrashed()->get();
        return $users;
    }
    public function find($id){
        $user=User::findOrFail($id);
        return $user;
    }
    public function findOnlyTrashed($id){
        $user=User::onlyTrashed()->findOrFail($id);
        return $user;
    }
    public function store($request){
        $data=$request->validated();
        $password=Hash::make($data['password']);
        $data['password']=$password;
    
        $user= User::create($data);
        $user->roles()->attach($request->roles);//to create roles for a user
        $user->permissions()->attach($request->permissions);//to create permissions for a user
        return $user;
    
    }
    public function update($request,$id){
        $user=$this->find($id);
        $data= $request->validated();
        $password=Hash::make($data['password']);
        $data['password']=$password;
        $user->update($data);
        if($request->roles){
            $user->syncRoles($request->roles);//to update roles a user
        }
        if($request->permissions){
            $user->permissions()->sync($request->permissions);//to update permissions for a user

        }
        return $user;
    }
    public function restore($id){
        $user=User::onlyTrashed()->findOrFail($id);//get this user from trash 
        $user->restore();
        return $user;
    }
    public function restoreAll(){
       $user= User::onlyTrashed()->restore();//restore all users from trash into users table
        return $user;
    }
    public function destroy($id){
        
        $user=$this->find($id);
        $user->detachRoles($user->roles);
        $user->detachPermissions($user->permissions);
        $user->delete($user);
        return $user;
    }

    public function forceDelete($id){
        $user = $this->findOnlyTrashed($id);
        $user->forceDelete();
        return $user;
    }
    
}
