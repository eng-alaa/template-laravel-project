<?php
namespace Modules\Auth\Repositories\Role;

interface RoleRepositoryInterface
{
   public function all();
   public function trash();
   public function rolesUserByName($user);
   public function rolesUser($user);
   public function find($id);
   public function findOnlyTrashed($id);
   public function store($request);
   public function update($request,$id);
   public function restore($id);
   public function restoreAll();
   public function destroy($id);
   public function forceDelete($id);

}
