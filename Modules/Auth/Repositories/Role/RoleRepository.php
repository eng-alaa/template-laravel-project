<?php
namespace Modules\Auth\Repositories\Role;
use Modules\Auth\Entities\Role;
use Modules\Auth\Repositories\Role\RoleRepositoryInterface;
class RoleRepository implements RoleRepositoryInterface
{
   public function all(){
        $roles=Role::all();
        return  $roles;

    }
    
    public function rolesUserByName($user){
        $rolesUser= $user->roles->pluck('name')->toArray();
        return  $rolesUser;
    }


    public function rolesUser($user){
        $rolesUser= $user->roles->pluck('id')->toArray();
        return $rolesUser;
    }
    public function rolesPermission($permission){
        $rolesPermission= $permission->roles->pluck('id')->toArray();
        return $rolesPermission;
    }
    public  function trash(){
        $roles=Role::onlyTrashed()->get();
        return $roles;
    }
    public function find($id){
        $role=Role::findOrFail($id);
        return $role;
    }
    public function findOnlyTrashed($id){
        $role=Role::onlyTrashed()->findOrFail($id);
        return $role;
    }
    public function store($request){
        $data=$request->all();
       $role= Role::create($data);
       $role->permissions()->attach($data['permissions']);
       return $role;
    
    }
    public function update($request,$id){
        $role=Role::findOrFail($id);
        $role->update($request->all());
        if($request->permissions){
            $role->permissions()->sync($request->permissions);
        }
        return $role;
    }
    public function restore($id){
        $role=Role::onlyTrashed()->findOrFail($id);//get this role from trash 
        $role->restore();
        return $role;
    }
    public function restoreAll(){
       $role= Role::onlyTrashed()->restore();//restore all roles from trash into users table
        return $role;
    }
    public function destroy($id){
        
        $role=Role::findOrFail($id);
        $role->detachPermissions($role->permissions);
        $role->delete($role);
        return $role;
    }

    public function forceDelete($id){
        $role = $this->findOnlyTrashed($id);
        $role->forceDelete();
        return $role;
    }
}
