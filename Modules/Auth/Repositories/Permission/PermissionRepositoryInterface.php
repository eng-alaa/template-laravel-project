<?php
namespace Modules\Auth\Repositories\Permission;

interface PermissionRepositoryInterface
{
   public function all();
   function trash();
   public function PermissionsRole($role);
   public function PermissionsUser($role);
   public function find($id);
   public function findOnlyTrashed($id);
   public function store($request);
   public function update($request,$id);
   public function restore($id);
   public function restoreAll();
   public function destroy($id);
   public function forceDelete($id);
}
