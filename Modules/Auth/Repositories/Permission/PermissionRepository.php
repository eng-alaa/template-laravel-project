<?php
namespace Modules\Auth\Repositories\Permission;
use Modules\Auth\Entities\Permission;
use Modules\Auth\Repositories\Permission\PermissionRepositoryInterface;
class PermissionRepository implements PermissionRepositoryInterface
{
   public function all(){
        $permissions=Permission::all();
        return  $permissions;
    }
    public  function trash(){
        $permissions=Permission::onlyTrashed()->get();
        return $permissions;
    }
    public function PermissionsUser($user){
        $permissionsUser= $user->permissions->pluck('id')->toArray();
        return $permissionsUser;
    }
    public function PermissionsRole($role){
        $permissionsRole= $role->permissions->pluck('id')->toArray();
        return $permissionsRole;
    }
    
    public function find($id){
        $permission=Permission::findOrFail($id);
        return $permission;
    }
    public function findOnlyTrashed($id){
        $permission=Permission::onlyTrashed()->findOrFail($id);
        return $permission;
    }
    public function store($request){
        $data=$request->all();        
        $permission=  Permission::create($data);
        if($request->roles){
            $permission->roles()->attach($data['roles']);
        }
       return $permission;
    
    }
    public function update($request,$id){
        $permission=Permission::findOrFail($id);
        $permission->update($request->all());
        if($request->roles){
            $permission->roles()->sync($request->roles);
        }
        return $permission;
    }
    public function restore($id){
        $permission=Permission::onlyTrashed()->findOrFail($id);//get this Permission from trash 
        $permission->restore();
        return $permission;
    }
    public function restoreAll(){
       $permission= Permission::onlyTrashed()->restore();//restore all Permissions from trash into users table
        return $permission;
    }
    public function destroy($id){
        
        $permission=Permission::findOrFail($id);
        if($permission->roles){
            $permission->roles()->detach($permission->roles);
        }
        $permission->delete($permission);
        return $permission;
    }

    public function forceDelete($id){
        $permission = $this->findOnlyTrashed($id);
        $permission->forceDelete();
        return $permission;
    }
}
