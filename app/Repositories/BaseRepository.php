<?php
namespace App\Repositories;

use App\Repositories\BaseRepositoryInterface;
use Modules\Auth\Entities\User;
use Modules\Auth\Repositories\User\UserRepository;
use Modules\Auth\Repositories\Role\RoleRepository;
class BaseRepository implements BaseRepositoryInterface
{
    /**
     * @var UserRepository
     */
    protected $userRepo;
    /**
     * @var RoleRepository
     */
    protected $roleRepo;
    /**
     * BaseRepository constructor.
     *
     */
    public function __construct(UserRepository $userRepo, RoleRepository $roleRepo)
    {
        $this->userRepo = $userRepo;
        $this->roleRepo = $roleRepo;

    }
    public function redirectTo(){
        $user=User::find(auth()->user()->id);
        $rolesUser= $user->roles->pluck('name')->toArray();
        $existRoleSuperadministrator=  in_array('superadministrator',$rolesUser);
        if ( $existRoleSuperadministrator==true) {
            return route('admin.dashboard');
        }else{
            return route('home');
        }
    }

    public function authorize(){
        $user= $this->userRepo->find(auth()->user()->id);
        $rolesUser=$this->roleRepo->rolesUserByName($user);
        $existRoleSuperadministrator=  in_array('superadministrator',$rolesUser);
        if($existRoleSuperadministrator==true){
            return $existRoleSuperadministrator;
        }else{
            return false;
        }
    }

    public function getStatuses(){
        $statusCollection=collect([
            ['id'=>0,'status'=>'InActive'],
            ['id'=>1,'status'=>'Active']
           ]);
        return $statusCollection->pluck('id');
    }
}