<?php
namespace App\Repositories;

use Modules\Auth\Entities\User;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Container\Container;
use Rinvex\Repository\Exceptions\RepositoryException;
use Rinvex\Repository\Repositories\EloquentRepository;

interface BaseRepositoryInterface
{
   public function redirectTo();
   public function authorize();
   public function getStatuses();
}
