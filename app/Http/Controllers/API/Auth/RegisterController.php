<?php

namespace  App\Http\Controllers\API\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\LoginRequest;
use App\Models\Profile;
use App\Models\Role;
use App\Models\User;
use App\Providers\RouteServiceProvider;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rules;
use DB;
class RegisterController extends Controller
{

    /**
     * Display the registration view.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('auth.register');
    }

    /**
     * Handle an incoming registration request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        // dd($request->all());
       // try{
            $rules= [
                'email' => 'required|string|email|max:255|unique:users',
                'password' => ['required', 'confirmed', Rules\Password::defaults()],//paasword and password_confirmation
                'name' => 'required|string|max:255'
                // 'first_name' => 'required|string|max:255',
                // 'last_name' => 'required|string|max:255',
                // 'image' => 'required|string|max:255',
                // 'country' => 'required|string|max:255',
                // 'city' => 'required|string|max:255',
                // 'governorate' => 'required|string|max:255',
                // 'region' => 'required|string|max:255',
                // 'phone_no' => 'required|string|max:255|unique:users',
            ];

            $data=$request->all();
            $validator=Validator::make($data,$rules);
            if($validator->fails())
            {
                return response()->json([
                    'status'=>400,
                    'message'=>$validator->errors()
                    ]);
            }else{
                $user=new User();
                $user->email=$request->email;
                $user->password=Hash::make($request->password);
                $user->save();
                // $userProfile=new Profile();
               // dd($userProfile);
                // $userProfile->user_id=$user['id'];
                // $userProfile->first_name=$request->first_name;
                // $userProfile->last_name=$request->last_name;
                // $userProfile->image=$request->image;
                // $userProfile->country=$request->country;
                // $userProfile->city=$request->city;
                // $userProfile->governorate=$request->governorate;
                // $userProfile->region=$request->region;
                // $userProfile->phone_no=$request->phone_no;
                // $userProfile->save();
                DB::table('role_user')->insert(['role_id'=>3,'user_id'=>$user['id']]);//role id =3 -> user
                event(new Registered($user));
                return response()->json([
                    'status'=>200,
                    'message'=>'registered successfully, you can make login now'
                ]);
            }

        // }catch(\Exception $ex){
        //     return response()->json([
        //         'status'=>500,
        //         'message'=>'There is something wrong, please try again'
        //     ]);  
        // } 
    }
}
