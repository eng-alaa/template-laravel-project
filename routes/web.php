<?php

use Illuminate\Support\Facades\Route;

// use App\Http\Controllers\Backend\DashboardController;
use App\Http\Controllers\Frontend\HomeController;



Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
