<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Modules\Auth\Database\Seeders\UserSeeder;
use Modules\Auth\Database\Seeders\LaratrustSeeder;
use Modules\Auth\Database\Seeders\RoleSeeder;
use Modules\Auth\Database\Seeders\PermissionSeeder;
class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
     //   $this->call(LaratrustSeeder::class);//run LaratrustSeeder seeder that it in module LaratrustSeeder(user,role,permission)
    }
}
