<?php

return [
    /**
     * Control if the seeder should create a user per role while seeding the data.
     */
    'create_users' => true,

    /**
     * Control if all the laratrust tables should be truncated before running the seeder.
     */
    'truncate_tables' => true,

    'roles_structure' => [
        'superadministrator' => [
            'users' => 'r,sh,c,s,e,u,d',
            'roles' => 'r,sh,c,s,e,u,d',
            'permissions' => 'r,sh,c,s,e,u,d',
            'payments' => 'c,r,u,d',
            'profile' => 'r,u'
        ],
        'administrator' => [
            'profile' => 'r,u'
        ],
        'user' => [
            'profile' => 'r,u',
        ],
        'role_name' => [
            'module_1_name' => 'c,r,u,d',
        ]
    ],

    'permissions_map' => [
        'r' => 'read',
        'sh' => 'show',
        'c' => 'create',
        's' => 'store',
        'e' => 'edit',
        'u' => 'update',
        'd' => 'delete'
    ]
];
